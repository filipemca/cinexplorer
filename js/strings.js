myApp.constant("strings", {
    filter: {
        en_US: "Filter",
        pt_BR: "Filtrar"
    },
    error: {
        en_US: "Error",
        pt_BR: "Erro"
    },
    error_cin_connection: {
        en_US: "Couldn't complete the search. Verify your connection with CIn-UFPE.",
        pt_BR: "Não foi possível realizar a busca. Verifique se está conectado à rede do CIn"
    },
    loading: {
        en_US: "Loading",
        pt_BR: "Carregando"
    },
    home: {
        en_US: "Home",
        pt_BR: "Início"
    },
    academic: {
        en_US: "Teachers",
        pt_BR: "Docentes"
    },
    profile: {
        en_US: "Profile",
        pt_BR: "Perfil"
    },
    publications: {
        en_US: "Publications",
        pt_BR: "Publicações"
    },
    projects: {
        en_US: "Projects",
        pt_BR: "Projetos"
    },

    positions: {
        en_US: "Positions",
        pt_BR: "Cargos"
    },
    expertiseAreas: {
        en_US: "Areas of Expertise",
        pt_BR: "Áreas de Atuação"
    },
    interestAreas: {
        en_US: "Areas of Interest",
        pt_BR: "Áreas de Interesse"
    },
    publication: {
        en_US: "Publication",
        pt_BR: "Publicação"
    },
    news: {
        en_US: "News",
        pt_BR: "Notícias"
    },
    newsItem: {
        en_US: "News",
        pt_BR: "Notícia"
    },
    orientations: {
        en_US: "Orientations",
        pt_BR: "Orientações"
    },
    thesis: {
        en_US: "Thesis",
        pt_BR: "Dissertação/Tese"
    }
});
